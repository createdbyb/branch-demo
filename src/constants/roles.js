export default {
  ADMIN: 'Admin',
  SALES: 'Sales',
  APP_MANAGER: 'App Manager',
  MARKETING: 'Marketing',
  DEVELOPER: 'Developer',
};
