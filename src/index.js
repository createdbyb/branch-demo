import React from 'react';
import ReactDOM from 'react-dom';

import { ApolloProvider } from '@apollo/react-hooks';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import client from './services/client';
import App from './app';
import User from './pages/User';

import './styles/index.css';

const Root = () => (
  <ApolloProvider client={client}>
    <Router>
      <Switch>
        <Route exact path="/" render={() => <App />} />
        <Route path="/:email" render={() => <User />} />
      </Switch>
    </Router>
  </ApolloProvider>
);

ReactDOM.render(<Root />, document.getElementById('root'));
