import React, { useState, useEffect } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { Link } from 'react-router-dom';

import classnames from 'classnames';

import ROLES from '../../constants/roles';
import { ALL_USERS_QUERY, DELETE_USERS_MUTATION } from '../../constants/queries';
import Row from '../../components/Row';
import Header from '../../components/Header';
import Button from '../../components/Button';

import styles from './styles.module.css';

const Home = () => {
  const [selectedUsers, setSelectedUsers] = useState(new Set());

  const { loading, error, data } = useQuery(ALL_USERS_QUERY);

  const [deleteUsers, { data: deleteUserData, loading: deleteUserLoading }] = useMutation(
    DELETE_USERS_MUTATION,
    {
      refetchQueries: [{ query: ALL_USERS_QUERY }],
    }
  );

  useEffect(() => {
    if (deleteUserData) {
      setSelectedUsers(new Set());
    }
  }, [deleteUserData]);

  const selectUser = (checked, email) => {
    if (checked) {
      selectedUsers.add(email);
    } else {
      selectedUsers.delete(email);
    }
    setSelectedUsers(new Set(selectedUsers));
  };

  const users = data?.allUsers;
  if (loading || deleteUserLoading) {
    return <p>Loading...</p>;
  }

  if (error) {
    return <p>Error: {JSON.stringify(error)}</p>;
  }

  return (
    <div className={styles.container}>
      <Header
        title="Users"
        button={
          <Button
            text="Delete"
            variant="delete"
            disabled={!selectedUsers.size}
            onClick={() => {
              deleteUsers({ variables: { emails: Array.from(selectedUsers) } });
            }}
          />
        }
      />
      <div className={styles.tableHeader}>
        <div className={styles.filler}></div>
        <div className={styles.headerItem}>EMAIL</div>
        <div className={styles.headerItem}>NAME</div>
        <div className={styles.headerItem}>ROLE</div>
      </div>
      {users.map((user) => (
        <Row key={user.email}>
          <input
            type="checkbox"
            className={styles.filler}
            onChange={({ target: { checked } }) => selectUser(checked, user.email)}
          />
          <Link
            to={`/${user.email}`}
            className={classnames(styles.rowItem, {
              [styles.active]: selectedUsers.has(user.email),
            })}
          >
            <div>{user.email}</div>
          </Link>
          <div className={styles.rowItem}>{user.name}</div>
          <div className={styles.rowItem}>{ROLES[user.role]}</div>
        </Row>
      ))}
    </div>
  );
};

export default Home;
