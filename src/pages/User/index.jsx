import React, { useState, useEffect } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { useParams, useHistory } from 'react-router-dom';

import ROLES from '../../constants/roles';
import { UPDATE_USER_MUTATION, USER_QUERY, ALL_USERS_QUERY } from '../../constants/queries';
import Header from '../../components/Header';
import Button from '../../components/Button';

import styles from './styles.module.css';

export const User = () => {
  const { email } = useParams();
  const history = useHistory();
  const { loading, error, data } = useQuery(USER_QUERY, {
    variables: { email },
    fetchPolicy: 'cache-and-network',
  });

  const [updateUser, { loading: updateUserLoading, data: updateUserData }] = useMutation(
    UPDATE_USER_MUTATION,
    {
      refetchQueries: [{ query: ALL_USERS_QUERY }],
    }
  );

  const [user, setUser] = useState(data?.user);

  useEffect(() => {
    setUser(data?.user);
  }, [data]);

  useEffect(() => {
    if (updateUserData) {
      history.push('/');
    }
  }, [updateUserData]);

  if (loading || updateUserLoading) {
    return <h1>Loading...</h1>;
  }

  if (error) {
    return <p>Error: {JSON.stringify(error)}</p>;
  }
  return (
    <div className={styles.container}>
      <Header
        title={user?.email}
        button={
          <Button
            text="Save"
            variant="save"
            onClick={() =>
              updateUser({
                variables: { email, newAttributes: { role: user.role, name: user.name } },
              })
            }
          />
        }
      />
      <div className={styles.innerContainer}>
        <div className={styles.inputSection}>
          <span>Name</span>
          <input
            className={styles.inputBox}
            type="text"
            value={user?.name}
            onChange={(e) => setUser({ ...user, name: e.target.value })}
          />
        </div>
        <div className={styles.roles}>
          {Object.entries(ROLES).map((role) => (
            <div key={role} className={styles.roleContent}>
              <input
                type="radio"
                value={role[0]}
                checked={role[0] === user?.role}
                onChange={(e) => setUser({ ...user, role: e.target.value })}
              />{' '}
              <span className={styles.roleText}>{role[1]}</span>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default User;
