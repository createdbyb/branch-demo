import React from 'react';

import styles from './styles.module.css';

const Header = ({ title, button }) => (
  <div className={styles.header}>
    <h1>{title}</h1>
    {button}
  </div>
);

export default Header;
