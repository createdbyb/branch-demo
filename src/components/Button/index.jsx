import React from 'react';

import classnames from 'classnames';

import styles from './styles.module.css';

const Button = ({ variant, text, disabled, onClick }) => (
  <button
    type="button"
    onClick={onClick}
    className={classnames(styles.button, {
      [styles.buttonSave]: variant === 'save',
      [styles.buttonDelete]: variant === 'delete',
    })}
    disabled={disabled}
  >
    {text}
  </button>
);

export default Button;
